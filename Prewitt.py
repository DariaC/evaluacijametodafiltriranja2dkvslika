import numpy as np    
import cv2
from skimage.metrics import peak_signal_noise_ratio
from skimage import filters

coronal = cv2.imread('testSLice.jpg', 0)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)


noise = gaussian_noise(coronal, 0, 25)
noise_psnr = peak_signal_noise_ratio(coronal, noise)
noise_mse = np.square(np.subtract(coronal, noise)).mean()

prewitt_clean = filters.prewitt(coronal, mask = np.ones((363, 512), bool))*np.sqrt(2)
prewitt_cleaned = filters.prewitt(noise, mask = np.ones((363, 512), bool)) * np.sqrt(2)

cv2.imshow('prewitt cleaned 1', prewitt_cleaned)
cv2.imshow('prewitt_onClean 1', prewitt_clean)
cv2.imwrite('prewitt1.png', prewitt_cleaned*200)
cv2.imwrite('prewitt_onClean1.png', prewitt_clean*200)

prewitt_psnr = peak_signal_noise_ratio(coronal, prewitt_cleaned)
prewitt_mse = np.square(np.subtract(coronal, prewitt_cleaned)).mean()

prewitt_psnr2 = peak_signal_noise_ratio(coronal, prewitt_clean)
prewitt_mse2 = np.square(np.subtract(coronal, prewitt_clean)).mean()

print("Psnr normal vs noisy: ", noise_psnr)
print("Psnr normal vs prewitt filter: ", prewitt_psnr)
print("Psnr normal vs normal + Prewitt: ", prewitt_psnr2)

print("Mse normal vs noisy: ", noise_mse)
print("Mse normal vs prewitt filter: ", prewitt_mse)
print("Mse normal vs normal + Prewitt: ", prewitt_mse2)

# drugi način dobivanja prewitt filterom
# kernelX = np.array([[1,1,1], [0,0,0], [-1,-1,-1]])
# kernelY = np.array([[-1,0,1], [0,0,0], [-1,0,-1]])
# PrewittX = cv2.filter2D(gaussian_noise, -1, kernelX)
# PrewittY = cv2.filter2D(gaussian_noise, -1, kernelY)
# PrewittXY = PrewittX +PrewittY
# prewitt_clean_XY = cv2.filter2D(coronal, -1, kernelX) +cv2.filter2D(coronal, -1, kernelY)
# cv2.imwrite('prewitt2.jpg', PrewittXY)
# cv2.imshow('prewitt 2', PrewittXY)
# cv2.imwrite('prewitt_onClean2.jpg', prewitt_clean_XY)
# cv2.imshow('prewitt_onClean 2', prewitt_clean_XY)

cv2.waitKey(0)
cv2.destroyAllWindows()

