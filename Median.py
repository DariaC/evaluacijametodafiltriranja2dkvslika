import numpy as np    
import cv2
from skimage.metrics import peak_signal_noise_ratio

coronal = cv2.imread('testSLice.jpg', 0)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

## tested with this also, works same
def med_filter(image, radius):
    imgcopy=image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            imgcopy[i][j]=np.median(imgcopy[i:i+radius, j:j+radius])
    return imgcopy

noise = gaussian_noise(coronal, 0, 25)
noise_psnr = peak_signal_noise_ratio(coronal, noise)
noise_mse = np.square(np.subtract(coronal, noise)).mean()

median_filtered = cv2.medianBlur(noise, 5)
cv2.imwrite("median.jpg", median_filtered)
median_cleaned_psnr = peak_signal_noise_ratio(coronal, median_filtered)
median_cleaned_mse = np.square(np.subtract(coronal, median_filtered)).mean()

print("Psnr normal vs noisy: ", noise_psnr)
print("Mse normal vs noisy: ", noise_mse)

print("Psnr normal vs median filter: ", median_cleaned_psnr)
print("Mse normal vs median filter: ", median_cleaned_mse)

z = 0
all_psnr_x = []
all_mse_x = []
all_noise_psnr_x = []
all_noise_mse_x = []

for i in range(10):
    z = (i)
    read = cv2.imread("x00"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_x.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read, noise)).mean()
    all_noise_mse_x.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_x.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_x.append(current_mse_filtered)
    
for i in range(90):
    z = i
    z = z + 10
    read = cv2.imread("x0"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_x.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read,noise)).mean()
    all_noise_mse_x.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_x.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_x.append(current_mse_filtered)
    
for i in range(412):
    z = i
    z = z + 100
    cv2.imread("x"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_x.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read,noise)).mean()
    all_noise_mse_x.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_x.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_x.append(current_mse_filtered)
    
    
avg_all_noise_psnr_x = sum(all_noise_psnr_x)/len(all_noise_psnr_x)
avg_all_noise_mse_x = sum(all_noise_mse_x)/len(all_noise_mse_x)
avg_all_psnr_x = sum(all_psnr_x)/len(all_psnr_x)
avg_all_mse_x =sum(all_mse_x)/len(all_mse_x)

print("Average pnsr of x slices - noise: " + str(avg_all_noise_psnr_x))
print("Average mse of x slices - noise: " + str(avg_all_noise_mse_x))
print("Average pnsr of x slices - filtered: " + str(avg_all_psnr_x))
print("Average mse of x slices - filtered: " + str(avg_all_mse_x))
    
z = 0
all_psnr_y = []
all_mse_y = []
all_noise_psnr_y = []
all_noise_mse_y = []

for i in range(10):
    z = (i)
    read = cv2.imread("y00"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_y.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read,noise)).mean()
    all_noise_mse_y.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_y.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_y.append(current_mse_filtered)
    
for i in range(90):
    z = i
    z = z + 10
    read = cv2.imread("y0"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_x.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read, noise)).mean()
    all_noise_mse_x.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_x.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_x.append(current_mse_filtered)
    
for i in range(412):
    z = i
    z = z + 100
    cv2.imread("y"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_x.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read,noise)).mean()
    all_noise_mse_x.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_x.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_x.append(current_mse_filtered)
    
    
avg_all_noise_psnr_y = sum(all_noise_psnr_y)/len(all_noise_psnr_y)
avg_all_noise_mse_y = sum(all_noise_mse_y)/len(all_noise_mse_y)
avg_all_psnr_y = sum(all_psnr_y)/len(all_psnr_y)
avg_all_mse_y =sum(all_mse_y)/len(all_mse_y)

print("Average pnsr of y slices - noise: " + str(avg_all_noise_psnr_y))
print("Average mse of y slices - noise: " + str(avg_all_noise_mse_y))
print("Average pnsr of y slices - filtered: " + str(avg_all_psnr_y))
print("Average mse of y slices - filtered: " + str(avg_all_mse_y))
    
z = 0
all_psnr_z = []
all_mse_z = []
all_noise_psnr_z = []
all_noise_mse_z = []

for i in range(10):
    z = (i)
    read = cv2.imread("z00"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_z.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read,noise)).mean()
    all_noise_mse_z.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_z.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_z.append(current_mse_filtered)
    
for i in range(90):
    z = i
    z = z + 10
    read = cv2.imread("z0"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_z.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read, noise)).mean()
    all_noise_mse_z.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_z.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_z.append(current_mse_filtered)
    
for i in range(263):
    z = i
    z = z + 100
    cv2.imread("z"+ str(z) + ".jpg", 0)
    noise = gaussian_noise(read, 0, 25)
    filtered = cv2.medianBlur(noise, 5)
    current_psnr_noise = peak_signal_noise_ratio(read, noise)
    all_noise_psnr_z.append(current_psnr_noise)
    current_mse_noise = np.square(np.subtract(read,noise)).mean()
    all_noise_mse_z.append(current_mse_noise)
    current_psnr_filtered = peak_signal_noise_ratio(read, filtered)
    all_psnr_z.append(current_psnr_filtered)
    current_mse_filtered = np.square(np.subtract(read, filtered)).mean()
    all_mse_z.append(current_mse_filtered)
    
    
avg_all_noise_psnr_z = sum(all_noise_psnr_z)/len(all_noise_psnr_z)
avg_all_noise_mse_z = sum(all_noise_mse_z)/len(all_noise_mse_z)
avg_all_psnr_z = sum(all_psnr_z)/len(all_psnr_z)
avg_all_mse_z = sum(all_mse_z)/len(all_mse_z)

print("Average pnsr of z slices - noise: " + str(avg_all_noise_psnr_z))
print("Average mse of z slices - noise: " + str(avg_all_noise_mse_z))
print("Average pnsr of z slices - filtered: " + str(avg_all_psnr_z))
print("Average mse of z slices - filtered: " + str(avg_all_mse_z))

avg_all_noise_psnr = ((avg_all_noise_psnr_z + avg_all_noise_psnr_y + avg_all_noise_psnr_x)/3)
avg_all_noise_mse = ((avg_all_noise_mse_z + avg_all_noise_mse_y + avg_all_noise_mse_x)/3)
avg_all_psnr = ((avg_all_psnr_z + avg_all_psnr_y + avg_all_psnr_x)/3)
avg_all_mse = ((avg_all_mse_z + avg_all_mse_y + avg_all_mse_x)/3)

print("Average pnsr of all slices - noise: " + str(avg_all_noise_psnr))
print("Average mse of all slices - noise: " + str(avg_all_noise_mse))
print("Average pnsr of all slices - filtered: " + str(avg_all_psnr))
print("Average mse of all slices - filtered: " + str(avg_all_mse))